#!/usr/bin/python
import json
import random
import sys
import datetime
import time
import requests
from threading import Thread
import logging

from faker import Faker
fake = Faker()

MIN_INT = ((-1)*sys.maxint)-1
MAX_INT = sys.maxint

logger = logging.getLogger('')
console = logging.StreamHandler()
logger.addHandler(console)
logger.setLevel(20)


class Report(object):
    alogorithms = list(range(1, 8))
    activity_types = [1, 2, 4, 8, 16, 32]
    visit_time = datetime.datetime(year=2018, month=1, day=1, hour=0, minute=0, second=0)
    activity_time = datetime.datetime(year=2018, month=1, day=1, hour=1, minute=0, second=0)
    visits = [] 
    activities = []

    def __init__(self, fake_send):
        """Assigns a client id for reporting object.

        Args:
            fake_send - do not send actually data to process.
        """
        self.client_id = fake.uuid4()
        self.fake_send = fake_send

    def time_tuple(self, start_time, time_offset):
        """Returns a tuple with time range.

        Args:
            start_time - datetime object of time range start.

            time_offset - time offset for particular report.
        """
        enter_time = time.mktime(start_time.timetuple()) * 1e3 + start_time.microsecond / 1e3
        time_offset = start_time + datetime.timedelta(minutes=time_offset)
        exit_time = time.mktime(time_offset.timetuple()) * 1e3 + time_offset.microsecond / 1e3
        return (int(enter_time), int(exit_time))

    def visit_data(self):
        """Returns a data dict for single visit report."""
        enter_time, exit_time = self.time_tuple(start_time=self.visit_time, time_offset=40)
        latitude, longitude = fake.latlng()
        data = {
            "DataVer": 1,
            "UserId": self.client_id,
            "EnterTime": enter_time,
            "ExitTime": exit_time,
            "AlgorithmType": random.randint(1, 7),
            "PoiId": random.randint(MIN_INT, MAX_INT),
            "Latitude": float(latitude),
            "Longitude": float(longitude),
        }
        return data

    def activity_data(self):
        """Returns a data dict for single activity report."""
        start_time, end_time = self.time_tuple(start_time=self.activity_time, time_offset=30)
        start_lat, start_long = fake.latlng()
        end_lat, end_long = fake.latlng()
        data = {
            "DataVer": 1,
            "UserId": self.client_id,
            "ActivityType": random.choice(self.activity_types),
            "StartTime": start_time,
            "EndTime": end_time,
            "StartLatitude": float(start_lat),
            "StartLongitude": float(start_long),
            "EndLatitude": float(end_lat),
            "EndLongitude": float(end_long),
        }
        return data

    def pack(self, data):
        """Returns a data packed into json package."""
        return json.dumps(data)

    def send(self, data, data_type):
        """Method sends a POST request to test server.

        Args:
            data - data to be sent.

            data_type - must be one of 'visit' or 'activity' key word.
        """
        scheme = "http"
        server_name = "127.0.0.1"
        port = "80"
        url = "%(scheme)s://%(server_name)s:%(port)s/api/%(data_type)s/v1/" % locals()

        logger.debug("\n%s\n", json.dumps(data))
        if not self.fake_send:
            logger.info("Really sending to %s data...", url)
            response = requests.post(url, json=data)
            return response
        else:
            logger.debug("Fake sending to %s data...", url)


class Worker(Thread):
    """Threading worker thread instance.
    
    Attrs:
        kill_received - Flag for worker halt.
    """
    kill_received = False

    def __init__(self, idle_time, step_time, fake_send, *args, **kwargs):
        super(Worker, self).__init__(*args, **kwargs)
        self.report = Report(fake_send=fake_send)
        self.idle_time = idle_time
        self.step_time = step_time

    def run(self):
        """Main thread process method."""
        visit_day = activity_day = 1
        # just assign it to variable to make shorter name
        report = self.report
        while not self.kill_received:
            need_idle = False

            # process visits
            report.visit_time += datetime.timedelta(seconds=self.step_time)
            report.visits.append(report.visit_data())
            if visit_day != report.visit_time.day:
                data = report.pack(report.visits)
                report.send(data=data, data_type="visit")
                report.visits = []
                need_idle = True

            # process activities
            report.activity_time += datetime.timedelta(seconds=self.step_time)
            report.activities.append(report.activity_data())
            if activity_day != report.activity_time.day:
                data = report.pack(report.activities)
                report.send(data=data, data_type="activity")
                report.activities = []
                need_idle = True

            # sleep for a while if required
            if need_idle:
                logger.debug("Sleeping for %s seconds...[Ctlr+C may be disabled]", self.idle_time)
                time.sleep(self.idle_time)

            visit_day = report.visit_time.day
            activity_day = report.activity_time.day


def run(threads_num, idle_time, step_time, fake_send, **kwargs):
    """Thread processing function."""
    def alive(threads):
        """Returns True if at least one thread is alive."""
        for thread in threads:
            if thread and thread.isAlive():
                return True
        return False

    threads = []
    for i in range(threads_num):
        thread = Worker(idle_time, step_time, fake_send)
        # start each worker thread
        thread.start()
        # collect theads into one list to manage them
        threads.append(thread)

    while alive(threads):
        try:
            # synchronization timeout of threads kill
            for thread in threads:
                if thread and thread.isAlive():
                    thread.join(1)

        except KeyboardInterrupt:
            # Ctrl-C handling and send kill to threads
            print "Sending kill to threads..."
            for thread in threads:
                thread.kill_received = True
            print "Process finished with Ctrl+C..."


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Anagog test task http(s) client.')
    parser.add_argument('-t', '--threads_num', dest="threads_num", type=int, default=1,
                        help="(int) number of threads.")
    parser.add_argument('-i', '--idle_time', dest="idle_time", type=float, default=0.0,
                        help="(float) idle timeout after each data collection loop pass.")
    parser.add_argument('-s', '--step_time', dest="step_time", type=int, default=2*60*60,
                        help="(int) step timeout in seconds.")
    parser.add_argument('-f', '--fake_send', dest="fake_send", action='store_true', default=False,
                        help="do not actually send reports.")
    parser.add_argument('-v', '--verbose', dest="verbose", action='store_true', default=False,
                        help="more informative output.")
    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(0)

    context = {
        "threads_num": args.threads_num,
        "idle_time": args.idle_time,
        "step_time": args.step_time,
        "fake_send": args.fake_send,
        "verbose": args.verbose,
    }

    msg = """
    Starting the process...
    Threads: %(threads_num)s
    Idle timeout: %(idle_time)s
    Step time: %(step_time)s
    Fake HTTP requests: %(fake_send)s
    Verbose output: %(verbose)s
    """
    logger.info(msg, context)
    run(**context)
