eval $(minikube docker-env)
docker build -t anator:latest minikube/server
docker build -t anator-kafka:latest minikube/anator-kafka
docker build -t anator-zookeeper:latest minikube/anator-zookeeper
kubectl apply -f minikube/anasync.yml
