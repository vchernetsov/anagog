flake8==3.5.0
marshmallow==2.15.4
pylint==2.1.1
tornado==5.1
kafka-python==1.4.4
