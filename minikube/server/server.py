"""This is main HTTP-server description"""

import json
import logging
from kafka import KafkaProducer
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.gen
from validators import VisitValidator
from validators import ActivityValidator

PRODUCER = KafkaProducer(bootstrap_servers=['kafka:9092'], value_serializer=lambda x: json.dumps(x).encode('utf-8'))
LOGGER = logging.getLogger('')
HTTP_PORT = 8000


class StatusHandler(tornado.web.RequestHandler):
    """Just stub handler replying to standard url that server is up and ready to respond."""

    @tornado.web.asynchronous
    def get(self):
        """Method serving get HTTP method of stub class."""
        try:
            KafkaProducer(bootstrap_servers=['kafka:9092'])
        except Exception as e:
            self.write("Kafka failed")  # ANAgog TORnado node
            self.finish()
            return

        self.write("ANATOR - OK")  # ANAgog TORnado node
        self.finish()


class BaseHandler(tornado.web.RequestHandler):
    """Base HTTP handler."""
    TOPIC_NAME = None
    VALIDATOR = None

    @tornado.gen.coroutine
    def write_kafka(self, topic, message):
        """Async method writing a message to correspondent topic."""
        f = PRODUCER.send(topic, value=message)
        raise tornado.gen.Return(f)

    @tornado.web.asynchronous
    def post(self):
        """Async method serving post HTTP method and writes request data to Kafka.
        It validates an input JSON data.
        If request data can not be validated, it will return HTTP 403 with a set of errors.
        """
        raw_data = tornado.escape.json_decode(self.request.body)
        data_dict = json.loads(raw_data)
        validator = self.VALIDATOR().load(data_dict, many=True)
        if not validator.errors:
            for x in data_dict:
                LOGGER.warning("Sending package to kafka %s", x)
                self.write_kafka(topic=self.TOPIC_NAME, message=x)
        else:
            self.set_header("Content-Type", "application/json")
            self.set_status(400)
            self.write(raw_data)
        self.finish()


class VisitHandler(BaseHandler):
    """Class serving visit dataset."""
    TOPIC_NAME = "visit-topic"
    VALIDATOR = VisitValidator


class ActivityHandler(BaseHandler):
    """Class serving activity dataset."""
    TOPIC_NAME = "activity-topic"
    VALIDATOR = ActivityValidator


URLS = [
    (r"/", StatusHandler),
    (r"/api/visit/v1/", VisitHandler),
    (r"/api/activity/v1/", ActivityHandler),
]


def main():
    """Main method to serve server."""
    LOGGER.warning("Starting anator HTTP server at port %s " % HTTP_PORT)
    application = tornado.web.Application(URLS, autoreload=True)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(HTTP_PORT)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
