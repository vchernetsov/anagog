"""This module keeps a validation classes."""

from marshmallow import Schema
from marshmallow import fields


class VisitValidator(Schema):
    """Class validates a visit action."""
    ALGORITHMS = list(range(1, 8))

    DataVer = fields.Int(required=True, choices=[1, ])
    UserId = fields.UUID(required=True)
    EnterTime = fields.Integer(required=True)
    ExitTime = fields.Integer(required=True)
    AlgorithmType = fields.Integer(required=True, choices=ALGORITHMS)
    PoiId = fields.Integer(required=True)
    Latitude = fields.Float(required=True)
    Longitude = fields.Float(required=True)


class ActivityValidator(Schema):
    """This class validates an activity action."""
    ACTIVITY_TYPES = [1, 2, 4, 8, 16, 32]

    DataVer = fields.Int(required=True, choices=[1, ])
    UserId = fields.UUID(required=True)
    ActivityType = fields.Integer(required=True, choices=ACTIVITY_TYPES)
    StartTime = fields.Integer(required=True)
    EndTime = fields.Integer(required=True)
    StartLatitude = fields.Float(required=True)
    StartLongitude = fields.Float(required=True)
    EndLatitude = fields.Float(required=True)
    EndLongitude = fields.Float(required=True)
