"""Data consumer."""

import asyncio
import datetime
import time
from aiokafka import AIOKafkaConsumer
from aiofile import AIOFile


TOPICS = {
    "visit-topic": asyncio.Queue(),
    "activity-topic": asyncio.Queue(),
}
PAGE_SIZE = 1000
DELTA = datetime.timedelta(minutes=2)
LOOP = asyncio.get_event_loop()
KAFKA_HOST = 'kafka:9092'
CONSUMER = AIOKafkaConsumer('visit-topic', 'activity-topic', loop=LOOP,
                            bootstrap_servers=KAFKA_HOST, auto_offset_reset='latest',
                            enable_auto_commit=True,
                            value_deserializer=lambda x: x.decode('utf-8'))

# pylint: disable=unused-argument
async def write_file(topic, data):
    """Async function writing file to disc."""
    data = "\n".join(data)
    # pylint: disable=possibly-unused-variable
    now = datetime.datetime.now()
    timestamp = now.strftime("%Y_%m_%d %H-%M-%S")
    name = "data/%(topic)s-%(timestamp)s.json" % locals()
    print('Writing file...', name)
    async with AIOFile(name, 'w+') as afp:
        await afp.write(data)
        await afp.fsync()


async def consume():
    """Async function consuming messages from kafka."""
    print("consume")
    await CONSUMER.start()
    try:
        async for msg in CONSUMER:
            TOPICS[msg.topic].put_nowait(msg.value)
            print("consumed: %s"  % msg.value)
    finally:
        await CONSUMER.stop()


async def chew(queue, topic):
    """Async function receive messages from consumer and writing them into the file."""
    print('chew')
    next_write = datetime.datetime.now() + DELTA
    while True:
        print('chewing (%s)...' % queue.qsize())
        now = datetime.datetime.now()
        print (next_write - now)
        if queue.qsize() >= PAGE_SIZE or now >= next_write:
            data = []
            for _ in range(PAGE_SIZE):
                try:
                    message = queue.get_nowait()
                    data.append(message)
                except asyncio.queues.QueueEmpty:
                    break
            await write_file(topic, data)
            next_write = now + DELTA
        await asyncio.sleep(1)


LOOP.create_task(consume())
LOOP.create_task(chew(TOPICS['visit-topic'], 'visit'))
LOOP.create_task(chew(TOPICS['activity-topic'], 'activity'))
LOOP.run_forever()
